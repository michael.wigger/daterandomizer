package ch.tje.DateRandomizer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainClass {

	public static int[] selectedYears = { 2010,2012,2014,2016,2018,2020};
	public static int[] selectWeekday = { Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, 
			Calendar.THURSDAY, Calendar.FRIDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY,
			Calendar.MONDAY, Calendar.TUESDAY
	};
	public static int lower_range_of_hour_interval =  8;
	public static int size_of_hour_interval =  8;
	
	
	public static void main(String[] args) {
		
		System.out.println("Purpose: Generate the random dates for several years");
		
		for (int years = 0; years < selectedYears.length;years++) {
			for (int months = Calendar.JANUARY; months <= Calendar.DECEMBER; months++) {
				int selectedWeekDay = selectWeekday[months];
				Date generatedDate = getGeneratedDate(selectedYears[years], months,selectedWeekDay);
				SimpleDateFormat formatter = new SimpleDateFormat("E dd.MM.yyyy HH:mm");
				System.out.println( formatter.format(generatedDate) );
			}
		}
		
		

	}

	/* This method generates a date according to the param year, month and the weekday. */

	private static Date getGeneratedDate(int year, int month, int selectedWeekDay) {
		int maxDaysInMonth = getMaxDaysInMonth(year,month);
		int getSelectedDays = getSelectedDaysAccordingToWeekday(year,month,maxDaysInMonth, selectedWeekDay);
		int getSelectedHour = generateRandomHour();
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, getSelectedDays);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.HOUR, getSelectedHour);
		calendar.set(Calendar.MINUTE, 0);
		return calendar.getTime();
	}
	
	private static int generateRandomHour() {
		return (int)Math.floor(Math.random()*size_of_hour_interval+lower_range_of_hour_interval);
	}


	/* Generates a random number and checks if the date really is the right weekday  */
	
	private static int getSelectedDaysAccordingToWeekday(int year, int month, int maxDaysInMonth, int selectedWeekDay) {
		int random_int = -1;
		do {
			random_int = (int)Math.floor(Math.random()*(maxDaysInMonth+1)+1);
			boolean isRightWeekday = checkWeekday(year, month,random_int, selectedWeekDay);
			if (isRightWeekday == true) break;
		} while (true);
 		
		return random_int;
	}


	/* checks if the date really is the right weekday  */
	
	private static boolean checkWeekday(int year, int month, int random_int, int selectedWeekDay) {
		Calendar calendar = Calendar.getInstance();
		 calendar.set(Calendar.DAY_OF_MONTH, random_int);
		 calendar.set(Calendar.MONTH, month);
		 calendar.set(Calendar.YEAR, year);
		 int weekday =  calendar.get(Calendar.DAY_OF_WEEK);
		return weekday == selectedWeekDay;
	}


	/* return the maximum number of day in this month  */
	
	private static int getMaxDaysInMonth(int year, int months) {
		Calendar calendar = Calendar.getInstance();
		 calendar.set(Calendar.DAY_OF_MONTH, 1);
		 calendar.set(Calendar.MONTH, months);
		 calendar.set(Calendar.YEAR, year);
	     int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	     return days;
	}

}
