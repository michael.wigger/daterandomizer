# DateRandomizer

The purpose of the project is the generation of the random datatime points.
With the selected years 2010,2012,2014,2016,2018 and 2028, we iterated 
over the month with preselected weekday. If the selected day of the month,
the program selects a hour out of a time range from 8am to 4pm. 

## Documentation

There is a short description of the software in the *doc* folder

## Software

The source is located in the *src* folder. My son, Lukas Wigger, prepared
a build pipeline to generated the jar file



